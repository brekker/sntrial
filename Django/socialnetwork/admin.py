from django.contrib import admin
from .models import Post


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    fields = ('content', 'user', 'like', 'dislike')
    list_display = ('id', 'user', 'like', 'dislike')