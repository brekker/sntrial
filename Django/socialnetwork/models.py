from django.db import models
from django.contrib.auth.models import User


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    content = models.CharField(max_length=400, blank=True)

    like = models.IntegerField(default=0)
    dislike = models.IntegerField(default=0)

    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.id)

    def add_like(self):
        self.like += 1;

    def add_dislike(self):
        self.dislike += 1;
