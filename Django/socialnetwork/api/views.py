from rest_framework import permissions
from rest_framework.generics import CreateAPIView
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from socialnetwork.models import Post
from .serializers import PostSerializer


class PostCreateView(CreateAPIView):
    serializer_class = PostSerializer
    permission_classes = (permissions.IsAuthenticated, )


@api_view()
@permission_classes((permissions.IsAuthenticated,))
def post_emotion(request, pk):
    post = Post.objects.get(id=pk)
    result = False
    if post:
        emotion = request.data.get('emotion')
        if emotion == 'like':
            post.add_like()
            post.save()
            result = True
        elif emotion =='dislike':
            post.add_dislike()
            post.save()
            result = True
    return Response( {'ok': result} )