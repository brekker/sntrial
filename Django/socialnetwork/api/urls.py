from socialnetwork.api.views import PostCreateView, post_emotion
from django.urls import path


urlpatterns = [
    path('post/create', PostCreateView.as_view(), name='create_post'),
    path('post/<int:pk>/emotion', post_emotion, name='post_emotion'),
]