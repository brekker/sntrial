import requests
import random
import string
from config_file import *


text = 'Lorem ipsum dolor sit amet consectetur adipiscing elit Sed ornare dolor elit id efficitur risus condimentum' \
       ' id Cras orci velit convallis nec enim id tempus vehicula quam Cras ultrices augue eget tincidunt sagittis' \
       ' Nam nisl urna laoreet id dignissim at faucibus et tellus Curabitur nec pulvinar nulla Nunc at libero' \
       ' fermentum semper nulla sit amet scelerisque massa Nullam nec nibh turpis Sed ultricies elementum magna Ut' \
       ' ut vulputate nibh quis accumsan nisi Maecenas commodo lacus orci, non varius nibh consequat eget Aliquam' \
       ' eleifend dui sit amet orci fermentum mattis Vestibulum finibus ante et metus tempor lobortis'


def generate_post():

    content = ''
    words = text.split(' ')
    for i in range(random.randint(5, len(words))):
        content += words[i] + " "
    result = {
        'content': content
    }
    return result


def randomString(stringLength=10):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def get_token(username, password):
    url = 'http://localhost:8000/api-token-auth/'
    post_data = {'username': username, 'password': password}
    try:
        r = requests.post(url, data=post_data)
        result = r.json().get('token')
    except:
        result = None
    return result


def registration(username, email, password):
    url = 'http://localhost:8000/rest-auth/registration/'
    post_data = {
            "username": username,
            "email": email,
            "password1": password,
            "password2": password,
        }
    try:
        r = requests.post(url, data=post_data)
        result = r.json().get('key')
    except:
        result = None
    return result


def create_post(item, token):
    url = 'http://127.0.0.1:8000/api/post/create'
    head = {'Authorization': 'token {}'.format(token)}
    try:
        r = requests.post(url, data=item, headers=head)
        result = r.json()
    except:
        result = None
    return result


def set_emotion(post_id, emotion, token):
    url = 'http://127.0.0.1:8000/api/post/{}/emotion'.format(post_id)
    head = {'Authorization': 'token {}'.format(token)}
    try:
        r = requests.get(url, data={'emotion': emotion}, headers=head)
        result = r.json()
    except:
        result = None
    return result


def run():
    token_list = []
    for i in range(number_of_users):
        username = 'user_b_' + randomString(6)
        password = randomString()
        email = username + '@gmail.com'

        key = registration(username, email, password)
        if key:
            print('User registration. Success. Key: ', key)
            token_list.append(key)
        else:
            print('User registration. Fail.')

    post_list = []
    for token in token_list:
        for i in range(max_posts_per_user):

            post = create_post(generate_post(), token)
            if post and post.get('id'):
                print('Create post. Success. id: ', post.get('id'))
                post_list.append(post.get('id'))
            else:
                print('Create post. Fail.')

    for token in token_list:
        for i in range(max_likes_per_user):
            post_id = random.choice(post_list)
            emotion = random.choice(['like', 'dislike'])
            result = set_emotion(post_id, emotion, token)
            if result and result.get('ok'):
                print(emotion + ' post_id ' + str(post_id) + '. Success.')
            else:
                print(emotion + ' post_id ' + str(post_id) + '. Fail.')